use graphql_client::GraphQLQuery;
use serde::{Deserialize, Deserializer};

// implementation of byte array
#[derive(Debug, Clone)]
pub struct Bytea {
	pub bytes: Vec<u8>,
}

// Hasura uses lowercase type name
#[allow(non_camel_case_types)]
type bytea = Bytea;

// implement deserializing \\x prefixed hexadecimal
impl<'de> Deserialize<'de> for Bytea {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: Deserializer<'de>,
	{
		// Deserialize as a string
		let hex_string = String::deserialize(deserializer)?;
		// Parse the hexadecimal string into a byte vector
		let bytes = hex::decode(&hex_string[2..]).map_err(serde::de::Error::custom)?;
		Ok(Bytea { bytes })
	}
}

// generate code for given graphql query
macro_rules! graphql_query {
	($name:ident) => {
		#[derive(GraphQLQuery)]
		#[graphql(
			schema_path = "res/indexer-schema.json",
			query_path = "res/indexer-queries.graphql"
		)]
		pub struct $name;
	};
}

// repeat generation for multiple queries
macro_rules! graphql_query_for {
    ( $($Name:ident),+ ) => {
       $( graphql_query!($Name); )+
    };
}

// generate code for all queries in indexer-queries.graphql
graphql_query_for!(
	IdentityNameByIndex,
	IdentityInfo,
	IdentityNameByPubkey,
	WasIdentityNameByPubkey,
	LatestBlock,
	BlockByNumber,
	GenesisHash,
	NamesByIndexes
);
