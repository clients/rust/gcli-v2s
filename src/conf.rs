use crate::entities::vault_account;
use crate::entities::vault_account::DbAccountId;
use crate::*;
use serde::{Deserialize, Serialize};

const APP_NAME: &str = "gcli";

/// defines structure of config file
#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
	/// duniter endpoint
	pub duniter_endpoint: String,
	/// indexer endpoint
	pub indexer_endpoint: String,
	/// user address
	/// to perform actions, user must provide secret
	pub address: Option<AccountId>,
}

impl std::default::Default for Config {
	fn default() -> Self {
		Self {
			duniter_endpoint: String::from(data::LOCAL_DUNITER_ENDPOINT),
			indexer_endpoint: String::from(data::LOCAL_INDEXER_ENDPOINT),
			address: None,
		}
	}
}

impl std::fmt::Display for Config {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let address = if let Some(address) = &self.address {
			format!("{}", address)
		} else {
			"(no address)".to_string()
		};
		writeln!(f, "Ğcli config")?;
		writeln!(f, "duniter endpoint {}", self.duniter_endpoint)?;
		writeln!(f, "indexer endpoint {}", self.indexer_endpoint)?;
		write!(f, "address {address}")
	}
}

/// load config file and manage error if could not
pub fn load_conf() -> Config {
	match confy::load(APP_NAME, None) {
		Ok(cfg) => cfg,
		Err(e) => {
			log::warn!(
				"met error while loading config file {}",
				confy::get_configuration_file_path(APP_NAME, None)
					.unwrap()
					.display()
			);
			log::error!("{:?}", e);
			log::info!("using default config instead");
			log::info!("call `config save` to overwrite");
			Config::default()
		}
	}
}

#[derive(Clone, Default, Debug, clap::Parser)]
pub enum Subcommand {
	#[default]
	/// Show config path
	Where,
	/// Show config
	Show,
	/// Save config as modified by command line arguments
	Save,
	/// Rest config to default
	Default,
}

/// handle conf command
pub async fn handle_command(data: Data, command: Subcommand) -> Result<(), GcliError> {
	// match subcommand
	match command {
		Subcommand::Where => {
			println!(
				"{}",
				confy::get_configuration_file_path(APP_NAME, None)?.display()
			);
		}
		Subcommand::Show => {
			println!("{}", data.cfg);
			if let Some(ref account_id) = data.cfg.address {
				if let Some(account) = vault_account::find_by_id(
					data.connect_db(),
					&DbAccountId::from(account_id.clone()),
				)
				.await?
				{
					println!("(Vault: {})", account);
				}
			}
		}
		Subcommand::Save => {
			save(&data.cfg);
		}
		Subcommand::Default => {
			confy::store(APP_NAME, None, Config::default()).expect("unable to write config");
		}
	};

	Ok(())
}

pub fn save(cfg: &Config) {
	confy::store(APP_NAME, None, cfg).expect("unable to write config");
	println!("Configuration updated!");
}
