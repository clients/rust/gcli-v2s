use crate::commands::vault;
use crate::entities::vault_account;
use crate::utils::GcliError;
use inquire::validator::{ErrorMessage, Validation};
use sea_orm::ConnectionTrait;

pub fn prompt_password() -> Result<String, GcliError> {
	prompt_password_query("Password")
}

pub fn prompt_password_confirm() -> Result<String, GcliError> {
	prompt_password_query_confirm("Password")
}

pub fn prompt_password_query(query: impl ToString) -> Result<String, GcliError> {
	inquire::Password::new(query.to_string().as_str())
		.without_confirmation()
		.prompt()
		.map_err(|e| GcliError::Input(e.to_string()))
}

pub fn prompt_seed() -> Result<String, GcliError> {
	inquire::Password::new("Seed:")
		.without_confirmation()
		.with_validator(|input: &str| {
			if input.chars().any(|c| !c.is_ascii_hexdigit()) {
				Ok(Validation::Invalid(
					"Seed value must only contain valid hexadecimal characters [0-9a-fA-F]".into(),
				))
			} else if input.len() < 64 || input.len() > 64 {
				Ok(Validation::Invalid(
					"Seed value must be 32 bytes in hexadecimal format (64 characters long)".into(),
				))
			} else {
				Ok(Validation::Valid)
			}
		})
		.prompt()
		.map_err(|e| GcliError::Input(e.to_string()))
}

pub fn prompt_password_query_confirm(query: impl ToString) -> Result<String, GcliError> {
	inquire::Password::new(query.to_string().as_str())
		.prompt()
		.map_err(|e| GcliError::Input(e.to_string()))
}

/// Prompt for a (direct) vault name (cannot contain derivation path)
///
/// Also preventing to use '<' and '>' as those are used in the display
pub async fn prompt_vault_name_and_check_availability<C>(
	db: &C,
	initial_name: Option<&String>,
) -> Result<Option<String>, GcliError>
where
	C: ConnectionTrait,
{
	loop {
		let mut text_inquire = inquire::Text::new("Name:").with_validator({
			|input: &str| {
				if input.contains('<') || input.contains('>') || input.contains('/') {
					return Ok(Validation::Invalid(
						"Name cannot contain characters '<', '>', '/'".into(),
					));
				}

				Ok(Validation::Valid)
			}
		});

		if let Some(initial_name) = initial_name {
			text_inquire = text_inquire.with_initial_value(initial_name);
		}

		let name = text_inquire
			.prompt()
			.map_err(|e| GcliError::Input(e.to_string()))?;

		let name = if name.trim().is_empty() {
			None
		} else {
			Some(name.trim().to_string())
		};

		let available =
			vault_account::check_name_available(db, initial_name, name.as_ref()).await?;

		if available {
			return Ok(name);
		}

		println!(
			"Name '{}' is already in use in the vault. Please choose another name.",
			name.unwrap()
		);
	}
}

/// Prompt for a derivation path
pub fn prompt_vault_derivation_path() -> Result<String, GcliError> {
	inquire::Text::new("Derivation path:")
		.with_validator(|input: &str| {
			if !input.starts_with("/") {
				Ok(Validation::Invalid(
					"derivation path needs to start with one or more '/'".into(),
				))
			} else {
				match vault::parse_prefix_and_derivation_path_from_suri(input.to_string()) {
					Ok(_) => Ok(Validation::Valid),
					Err(error) => {
						if let GcliError::Input(message) = error {
							Ok(Validation::Invalid(ErrorMessage::from(message)))
						} else {
							Ok(Validation::Invalid("Unknown error".into()))
						}
					}
				}
			}
		})
		.prompt()
		.map_err(|e| GcliError::Input(e.to_string()))
}

pub fn confirm_action(query: impl ToString) -> Result<bool, GcliError> {
	inquire::Confirm::new(query.to_string().as_str())
		.prompt()
		.map_err(|e| GcliError::Input(e.to_string()))
}

pub fn select_action(query: impl ToString, choices: Vec<&str>) -> Result<&str, GcliError> {
	inquire::Select::new(query.to_string().as_str(), choices)
		.prompt()
		.map_err(|e| GcliError::Input(e.to_string()))
}
