use crate::commands::cesium;
use crate::entities::vault_account;
use crate::entities::vault_account::AccountTreeNode;
use crate::keys::CryptoScheme;
use crate::utils::GcliError;
use comfy_table::{Cell, Table};
use std::cell::RefCell;
use std::rc::Rc;
use std::str;

#[deprecated(
	note = "Should be removed in a future version when db persistence of vault is present for a while"
)]
pub fn compute_vault_key_files_table(vault_key_addresses: &[String]) -> Result<Table, GcliError> {
	let mut table = Table::new();
	table.load_preset(comfy_table::presets::UTF8_BORDERS_ONLY);
	table.set_header(vec!["Key file"]);

	vault_key_addresses.iter().for_each(|address| {
		table.add_row(vec![Cell::new(address)]);
	});

	Ok(table)
}

pub fn compute_vault_accounts_table(
	account_tree_nodes: &[Rc<RefCell<AccountTreeNode>>],
) -> Result<Table, GcliError> {
	let mut table = Table::new();
	table.load_preset(comfy_table::presets::UTF8_BORDERS_ONLY);
	table.set_header(vec![
		"SS58 Address/G1v1 public key",
		"Crypto",
		"Path",
		"Name",
	]);

	for account_tree_node in account_tree_nodes {
		let _ = add_account_tree_node_to_table(&mut table, account_tree_node);
	}

	Ok(table)
}

fn add_account_tree_node_to_table(
	table: &mut Table,
	account_tree_node: &Rc<RefCell<AccountTreeNode>>,
) -> Result<(), GcliError> {
	let rows = compute_vault_accounts_row(account_tree_node)?;
	rows.iter().for_each(|row| {
		table.add_row(row.clone());
	});

	for child in &account_tree_node.borrow().children {
		let _ = add_account_tree_node_to_table(table, child);
	}

	Ok(())
}

/// Computes one or more row of the table for selected account_tree_node
///
/// For ed25519 keys, will display over 2 rows to also show the base 58 G1v1 public key
pub fn compute_vault_accounts_row(
	account_tree_node: &Rc<RefCell<AccountTreeNode>>,
) -> Result<Vec<Vec<Cell>>, GcliError> {
	let empty_string = "".to_string();

	let depth_account_tree_node = vault_account::count_depth_account_tree_node(account_tree_node);

	let name = if let Some(name) = account_tree_node.borrow().account.name.clone() {
		name
	} else if let Some(computed_name) =
		vault_account::compute_name_account_tree_node(account_tree_node)
	{
		format!("<{}>", computed_name)
	} else {
		empty_string.clone()
	};

	let account_tree_node = account_tree_node.borrow();

	let address = if depth_account_tree_node > 0 {
		let ancestors = "│ ".repeat(depth_account_tree_node - 1);
		format!("{}├ {}", ancestors, account_tree_node.account.address)
	} else {
		account_tree_node.account.address.to_string()
	};

	let mut rows: Vec<Vec<Cell>> = vec![];

	let (path, crypto) = if let Some(path) = account_tree_node.account.path.clone() {
		(path, empty_string.clone())
	} else {
		let crypto_scheme = CryptoScheme::from(account_tree_node.account.crypto_scheme.unwrap());

		// Adding 2nd row for G1v1 public key
		if CryptoScheme::Ed25519 == crypto_scheme {
			rows.push(vec![Cell::new(format!(
				"└ G1v1: {}",
				cesium::compute_g1v1_public_key_from_ed25519_account_id(
					&account_tree_node.account.address.0
				)
			))]);
		}

		let crypto_scheme_str: &str = crypto_scheme.into();
		(
			format!("<{}>", account_tree_node.account.account_type()),
			crypto_scheme_str.to_string(),
		)
	};

	// Adding 1st row
	rows.insert(
		0,
		vec![
			Cell::new(&address),
			Cell::new(crypto),
			Cell::new(&path),
			Cell::new(&name),
		],
	);

	Ok(rows)
}

#[cfg(test)]
mod tests {
	mod vault_accounts_table_tests {
		use crate::commands::vault::display::compute_vault_accounts_table;
		use crate::entities::vault_account::tests::account_tree_node_tests::{
			mother_account_tree_node, mother_g1v1_account_tree_node,
		};
		use indoc::indoc;

		#[test]
		fn test_compute_vault_accounts_table_empty() {
			let table = compute_vault_accounts_table(&[]).unwrap();

			let expected_table = indoc! {r#"
			┌─────────────────────────────────────────────────────┐
			│ SS58 Address/G1v1 public key   Crypto   Path   Name │
			╞═════════════════════════════════════════════════════╡
			└─────────────────────────────────────────────────────┘"#
			};

			assert_eq!(table.to_string(), expected_table);
		}

		#[test]
		fn test_compute_vault_accounts_table() {
			let account_tree_node = mother_account_tree_node();
			let g1v1_account_tree_node = mother_g1v1_account_tree_node();

			let table =
				compute_vault_accounts_table(&[account_tree_node, g1v1_account_tree_node]).unwrap();

			let expected_table = indoc! {r#"
			┌──────────────────────────────────────────────────────────────────────────────────────────┐
			│ SS58 Address/G1v1 public key                           Crypto    Path     Name           │
			╞══════════════════════════════════════════════════════════════════════════════════════════╡
			│ 5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV       sr25519   <Base>   Mother         │
			│ ├ 5D34dL5prEUaGNQtPPZ3yN5Y6BnkfXunKXXz6fo7ZJbLwRRH               //0      Child 1        │
			│ │ ├ 5Fh5PLQNt1xuEXm71dfDtQdnwceSew4oHewWBLsWAkKspV7d             //0      Grandchild 1   │
			│ ├ 5GBNeWRhZc2jXu7D55rBimKYDk8PGk8itRYFTPfC8RJLKG5o               //1      <Mother//1>    │
			│ │ ├ 5CvdJuB9HLXSi5FS9LW57cyHF13iCv5HDimo2C45KxnxriCT             //1      <Mother//1//1> │
			│ 5ET2jhgJFoNQUpgfdSkdwftK8DKWdqZ1FKm5GKWdPfMWhPr4       ed25519   <Base>   MotherG1v1     │
			│ └ G1v1: 86pW1doyJPVH3jeDPZNQa1UZFBo5zcdvHERcaeE758W7                                     │
			└──────────────────────────────────────────────────────────────────────────────────────────┘"#
			};

			assert_eq!(table.to_string(), expected_table);
		}

		#[test]
		fn test_compute_vault_accounts_table_partial() {
			let mother = mother_account_tree_node();
			let child1 = mother.borrow().children[0].clone();

			let table = compute_vault_accounts_table(&[child1]).unwrap();

			let expected_table = indoc! {r#"
			┌─────────────────────────────────────────────────────────────────────────────────────┐
			│ SS58 Address/G1v1 public key                           Crypto   Path   Name         │
			╞═════════════════════════════════════════════════════════════════════════════════════╡
			│ ├ 5D34dL5prEUaGNQtPPZ3yN5Y6BnkfXunKXXz6fo7ZJbLwRRH              //0    Child 1      │
			│ │ ├ 5Fh5PLQNt1xuEXm71dfDtQdnwceSew4oHewWBLsWAkKspV7d            //0    Grandchild 1 │
			└─────────────────────────────────────────────────────────────────────────────────────┘"#
			};

			assert_eq!(table.to_string(), expected_table);
		}
	}
}
