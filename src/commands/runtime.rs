use crate::*;

pub async fn runtime_info(data: Data) {
	let api = data.client();
	let consts = runtime::constants();
	// get constant u32 value
	let getu32 = |c| api.constants().at(&c).unwrap();
	// get constant u64 value
	let getu64 = |c| api.constants().at(&c).unwrap();
	// get constant perbill value
	let getp = |c| api.constants().at(&c).unwrap();
	// get formatted currency value
	let getf = |c| data.format_balance(api.constants().at(&c).unwrap());

	// identity
	println!("--- identity ---");
	println!(
		"confirm period: {} blocks",
		getu32(consts.identity().confirm_period())
	);
	println!(
		"validation period: {} blocks",
		getu32(consts.identity().validation_period())
	);
	println!(
		"autorevocation period: {} blocks",
		getu32(consts.identity().autorevocation_period())
	);
	println!(
		"deletion period: {} blocks",
		getu32(consts.identity().deletion_period())
	);
	println!(
		"change owner key period: {} blocks",
		getu32(consts.identity().change_owner_key_period())
	);
	println!(
		"identity creation period: {} blocks",
		getu32(consts.identity().idty_creation_period())
	);
	// certification
	println!("--- certification ---");
	println!(
		"certification period: {} blocks",
		getu32(consts.certification().cert_period())
	);
	println!(
		"max certs by issuer: {}",
		getu32(consts.certification().max_by_issuer())
	);
	println!(
		"min received cert to issue cert: {}",
		getu32(
			consts
				.certification()
				.min_received_cert_to_be_able_to_issue_cert()
		)
	);
	println!(
		"certification validity: {} blocks",
		getu32(consts.certification().validity_period())
	);
	// wot
	println!("--- wot ---");
	println!(
		"first issuable on: {}",
		getu32(consts.wot().first_issuable_on())
	);
	println!(
		"min cert for membership: {}",
		getu32(consts.wot().min_cert_for_membership())
	);
	println!(
		"min cert for create identity: {}",
		getu32(consts.wot().min_cert_for_create_idty_right())
	);
	// membership
	println!("--- membership ---");
	println!(
		"membership validity: {} blocks",
		getu32(consts.membership().membership_period())
	);
	// smith members
	println!("--- smith members ---");
	println!(
		"max certs by issuer: {}",
		getu32(consts.smith_members().max_by_issuer())
	);
	println!(
		"min cert for membership: {}",
		getu32(consts.smith_members().min_cert_for_membership())
	);
	println!(
		"smith inactivity max duration: {}",
		getu32(consts.smith_members().smith_inactivity_max_duration())
	);
	// todo membership renewal period
	// distance
	println!("--- distance ---");
	println!(
		"max referee distance: {}",
		getu32(consts.distance().max_referee_distance())
	);
	println!(
		"min accessible referees: {:?}",
		getp(consts.distance().min_accessible_referees())
	);
	println!(
		"distance evaluation price: {}",
		getf(consts.distance().evaluation_price())
	);
	// currency
	println!("--- currency ---");
	println!(
		"existential deposit: {}",
		getf(consts.balances().existential_deposit())
	);
	// provide randomness
	println!("--- provide randomness ---");
	println!(
		"max requests: {}",
		getu32(consts.provide_randomness().max_requests())
	);
	println!(
		"request price: {}",
		getf(consts.provide_randomness().request_price())
	);
	// universal dividend
	println!("--- universal dividend ---");
	println!(
		"max past reevals: {}",
		getu32(consts.universal_dividend().max_past_reeval())
	);
	println!(
		"square money growth rate: {:?}",
		getp(consts.universal_dividend().square_money_growth_rate())
	);
	println!(
		"UD creation period: {}",
		getu64(consts.universal_dividend().ud_creation_period())
	);
	println!(
		"UD reeval period: {}",
		getu64(consts.universal_dividend().ud_reeval_period())
	);
	// todo treasury, technical committee, transaction payment, authority members
	// consts.system().ss58_prefix()
}
