use crate::*;

/// submit a certification and track progress
pub async fn certify(data: &Data, target: IdtyId) -> Result<(), subxt::Error> {
	submit_call_and_look_event::<
		runtime::certification::events::CertAdded,
		StaticPayload<runtime::certification::calls::types::AddCert>,
	>(data, &runtime::tx().certification().add_cert(target))
	.await
}

/// renew certification
pub async fn renew(data: &Data, target: IdtyId) -> Result<(), subxt::Error> {
	submit_call_and_look_event::<
		runtime::certification::events::CertRenewed,
		StaticPayload<runtime::certification::calls::types::RenewCert>,
	>(data, &runtime::tx().certification().renew_cert(target))
	.await
}
