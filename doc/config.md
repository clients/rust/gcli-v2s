# Ğcli config 

Some Ğcli commands require to have an address configured (for example to get account balance), some require to have a secret configured (to sign extrinsics).
Ğcli allows to save the address you want in a config file and to overwrite parts in command line arguments. Example:

```sh
# save Alice address to config file
gcli -S predefined -s Alice config save

# show config 
gcli config show
# [stdout]
# Ğcli config
# duniter endpoint ws://localhost:9944
# indexer endpoint http://localhost:8080/v1/graphql
# address 5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY

# use different address in command line
gcli --address 5Fxune7f71ZbpP2FoY3mhYcmM596Erhv1gRue4nsPwkxMR4n config show
# [stdout]
# Ğcli config
# duniter endpoint ws://localhost:9944
# indexer endpoint http://localhost:8080/v1/graphql
# address 5Fxune7f71ZbpP2FoY3mhYcmM596Erhv1gRue4nsPwkxMR4n
```

This also applies to rpc endpoint config (`--url`) or indexer config (`--indexer`).

## Using password encrypted vault

For convenience, default accounts are hardcoded in Ğcli without needing a password:

```sh
# when Alice address is stored in config file
gcli account transfer 1 5Fxune7f71ZbpP2FoY3mhYcmM596Erhv1gRue4nsPwkxMR4n
# no need for password to sign transaction
```

but in general usage, you want to store your secret in the local vault.

Here is an example that:
* adds a base _substrate_ account
* adds a derivation on it
* lists all available Addresses in the vault
* selects that derivation Address
* makes a transfer from selected Address

```sh
# add a new secret to the vault using substrate uri
gcli vault import
# [stdout]
# gcli vault import
# Substrate URI can be a mnemonic or a mini-secret ('0x' prefixed seed) together with optional derivation path
# > Substrate URI:  ********
# 
# Trying to import for SS58 address :'5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV'
# 
# Enter password to protect the key
# > Password ********
# (Optional) Enter a name for the vault entry
# > Name: test-mnemonic
# Creating <Base> account Base[address:5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV, name:Some("test-mnemonic"), crypto_scheme:Some(Sr25519)]
# Change done
```

```sh
# add a derivation to that Address and give it name "firstDerive"
gcli vault derive -a 5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV
# [stdout]
# Adding derivation to: Base[address:5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV, name:Some("test-mnemonic"), crypto_scheme:Some(Sr25519)]
# 
# Its parent hierarchy is this:
# ┌─────────────────────────────────────────────────────────────────────────────────────┐
# │ SS58 Address/G1v1 public key                       Crypto    Path     Name          │
# ╞═════════════════════════════════════════════════════════════════════════════════════╡
# │ 5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV   sr25519   <Base>   test-mnemonic │
# └─────────────────────────────────────────────────────────────────────────────────────┘
# 
# The linked <Base> account is Base[address:5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV, name:Some("test-mnemonic"), crypto_scheme:Some(Sr25519)]
# Enter password to decrypt the <Base> account key
# > Password ********
# 
# > Derivation path: //1
# 
# Trying to create derivation with address '5GBNeWRhZc2jXu7D55rBimKYDk8PGk8itRYFTPfC8RJLKG5o'
# 
# (Optional) Enter a name for the vault entry
# > Name: firstDerive
# Creating derivation account Derivation[address:5GBNeWRhZc2jXu7D55rBimKYDk8PGk8itRYFTPfC8RJLKG5o, name:Some("firstDerive"), path:Some("//1"), parent:Some("5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV")]
# Change done
```

```sh
# list available Addresses in the vault
gcli vault list all
# [stdout]
# available SS58 Addresses:
# ┌───────────────────────────────────────────────────────────────────────────────────────┐
# │ SS58 Address/G1v1 public key                         Crypto    Path     Name          │
# ╞═══════════════════════════════════════════════════════════════════════════════════════╡
# │ 5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV     sr25519   <Base>   test-mnemonic │
# │ ├ 5GBNeWRhZc2jXu7D55rBimKYDk8PGk8itRYFTPfC8RJLKG5o             //1      firstDerive   │
# └───────────────────────────────────────────────────────────────────────────────────────┘
```

```sh
# Use "firstDerive" derivation Address (changes the config Address)
gcli vault use -v firstDerive
# [stdout]
# Using: Derivation[address:5GBNeWRhZc2jXu7D55rBimKYDk8PGk8itRYFTPfC8RJLKG5o, name:Some("firstDerive"), path:Some("//1"), parent:Some("5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV")]
# Configuration updated!
```

After saving your secret to the vault, you will be able to unlock it with the password:

```sh
gcli account transfer 123 5Fxune7f71ZbpP2FoY3mhYcmM596Erhv1gRue4nsPwkxMR4n
# [stdout]
# (Vault: Derivation[address:5GBNeWRhZc2jXu7D55rBimKYDk8PGk8itRYFTPfC8RJLKG5o, name:Some("firstDerive"), path:Some("//1"), parent:Some("5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV")])
# > Password ********
# transaction submitted to the network, waiting 6 seconds...
# transfered 1.23 ĞD (5GBNeWRhZc2jXu7D55rBimKYDk8PGk8itRYFTPfC8RJLKG5o → 5Fxune7f71ZbpP2FoY3mhYcmM596Erhv1gRue4nsPwkxMR4n)
```

You can display the secret files location:

```sh
gcli vault where
# [stdout]
# /home/hugo/.local/share/gcli
```

---

There are more commands and options available; please use `--help` argument in any of the commands to get their description.

A few examples:

```sh
gcli --help
# [stdout]
# A command-line interface for Duniter v2s uses
# 
# Usage: gcli [OPTIONS] <COMMAND>
# 
# Commands:
#   account     Account (balance, transfer...)
#   identity    Identity (get, create, confirm, revoke...)
#   smith       Smith (certify, go-online, go-offline...)
#   tech        Tech (list members, proposals, vote...)
#   ud          Universal Dividend (claim...)
#   oneshot     Oneshot account (balance, create, consume...)
#   blockchain  Blockchain (current block, runtime info...)
#   indexer     Indexer (check, latest block)
#   config      Config (show, save...)
#   vault       Key management (import, generate, list...)
#   help        Print this message or the help of the given subcommand(s)
# 
# Options:
#   -i, --indexer <INDEXER>              Overwrite indexer endpoint
#       --no-indexer                     Do not use indexer
#   -s, --secret <SECRET>                Secret key or BIP39 mnemonic (only used when secret format is compatible) (eventually followed by derivation path)
#   -S, --secret-format <SECRET_FORMAT>  Secret key format (seed, substrate, g1v1)
#   -a <ADDRESS>                         SS58 Address
#   -v <NAME>                            Name of an SS58 Address in the vault
#   -u, --url <URL>                      Overwrite duniter websocket RPC endpoint
#   -n, --network <NETWORK>              Target network (local, gdev, gtest...)
#       --no-wait                        prevent waiting for extrinsic completion
#   -o, --output-format <OUTPUT_FORMAT>  Output format (human, json, ...) [default: human]
#   -h, --help                           Print help
#   -V, --version                        Print version
```

```sh
gcli vault --help
# [stdout]
# Key management (import, generate, list...)
# 
# Usage: gcli vault <COMMAND>
# 
# Commands:
#   list        List available SS58 Addresses in the vault
#   use         Use specific SS58 Address (changes the config Address)
#   generate    Generate a mnemonic
#   import      Import key from (substrate uri) or other format with interactive prompt
#   derive      Add a derivation to an existing SS58 Address
#   rename      Give a meaningful name to an SS58 Address in the vault
#   remove      Remove an SS58 Address from the vault together with its linked derivations
#   inspect     Inspect a vault entry, retrieving its Substrate URI (will provide more data in a future version)
#   list-files  (deprecated) List available key files (needs to be migrated with command `vault migrate` in order to use them)
#   migrate     (deprecated) Migrate old key files into db (will have to provide password for each key)
#   where       Show where vault db (or old keys) is stored
#   help        Print this message or the help of the given subcommand(s)
# 
# Options:
#   -h, --help  Print help
```

And some commands have a more detailed description when requesting `--help` from them:

```sh
gcli vault derive --help
# [stdout]
# Add a derivation to an existing SS58 Address.
# 
# Only "sr25519" crypto scheme is supported for derivations.
# 
# Use command `vault list base` to see available <Base> account and their crypto scheme
# And then use command 'vault list for' to find all accounts linked to that <Base> account.
# 
# Usage: gcli vault derive <-a <ADDRESS>|-v <NAME>>
# 
# Options:
#   -a <ADDRESS>
#           SS58 Address
# 
#   -v <NAME>
#           Name of an SS58 Address in the vault
# 
#   -h, --help
#           Print help (see a summary with '-h')
```