# Changelog

List of changelogs ordered from latest to oldest

## [0.4.0] - 2025-02-xx
### Changed
- Old key files cannot be used directly anymore, they have to be migrated to the SQLite file database.  You can use the following commands for that:
    - `gcli vault list-files` lists _old_ key files from previous versions
    - `gcli vault migrate` which allows to migrate _old_ key files into the vault database
- Some arguments and commands were adapted
    - Arguments
        - **SecretFormat** (`-S`): _cesium_ was renamed to _g1v1_ - but it will still work properly if using _cesium_
        - Several arguments were renamed for clarity.  Examples:
          - `<TARGET>` => `<USERNAME>`
          - `<ACCOUNT_ID>` => `<ADDRESS>`
    - Commands
        - `gcli vault import` now has an optional **SecretFormat** `-S` argument with _substrate_ as default value
            - It is now possible to import _substrate_ URI, _seed_, _g1v1_ id/secret (old "cesium") keys into the vault.
        - `gcli vault list` now has sub-commands and only shows Addresses from the SQLite file database

### Added
- Support for SQLite file database to persist vault data
- global `-v <NAME>` argument to select a vault Address by name (mutually exclusive with `-a <ADDRESS>` argument)
- Already mentioned in the "Changed" section above, but it is now possible to add different kind of keys to the vault when doing `vault import`
  - Default providing _substrate_ URI (or `-S substrate`): can be a mnemonic or a mini-secret ('0x' prefixed seed) together with optional derivation path; using Sr25519 crypto scheme
  - `-S seed`: using the 32 character hexadecimal secret seed; using Sr25519 crypto scheme
  - `-S g1v1` (or `-S cesium`): allows to input G1v1 `id` and `secret`; using Ed25519 crypto scheme
- Added possibility to `derive` non _g1v1_/_cesium_ Addresses (see `gcli vault derive`)
- Some commands were added
  - `gcli vault list all` List all \<Base\> SS58 Addresses and their linked derivations in the vault
  - `gcli vault list for` List \<Base\> and Derivation SS58 Addresses linked to the selected one
  - `gcli vault list base` List all \<Base\> SS58 Addresses in the vault
  - `gcli vault use` Use specific SS58 Address (changes the config Address)
  - `gcli vault derive` Add a derivation to an existing SS58 Address
  - `gcli vault rename` Give a meaningful name to an SS58 Address in the vault
  - `gcli vault remove` Remove an SS58 Address from the vault together with its linked derivations
  - `gcli vault inspect` Inspect a vault entry, retrieving its Substrate URI (will provide more data in a future version)
  - `gcli vault list-files` (deprecated) List available key files (needs to be migrated with command `vault migrate` in order to use them)
  - `gcli vault migrate` (deprecated) Migrate old key files into db (will have to provide password for each key)

### Fixed
- None

### Deprecated
- Two commands are now deprecated and will be removed in a future release
  - `gcli vault list-files`
  - `gcli vault migrate`

### Removed
- None

### CI/CD
- #45, !42:
  - In linux build job, switch to "non-slim" Debian image which provides `libssl-dev` now required
  - Introduce linux tests job (#46)
- !38: Build deb and macos pkgs in parallel

## [0.3.0] - 2024-10-10
### Added
- Previous version without changelog
