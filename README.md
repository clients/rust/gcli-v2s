# gcli

CLI client for [Duniter-V2S](https://git.duniter.org/nodes/rust/duniter-v2s/).

Using
- https://github.com/duniter/substrate
- https://github.com/duniter/subxt

## Install
Download from [release](https://git.duniter.org/clients/rust/gcli-v2s/-/releases) or build with `cargo build`.

On debian derived systems you can also : 
check on [release](https://git.duniter.org/clients/rust/gcli-v2s/-/releases) page for latest version then in bash cli :
```
GCLI_VERSION={change me with latest version (0.2.14 in 2024 summer) and remove {}}
wget https://git.duniter.org/clients/rust/gcli-v2s/-/jobs/artifacts/$GCLI_VERSION/raw/target/debian/gcli_$GCLI_VERSION-1_amd64.deb?job=build_linux
mv gcli_* gcli_package.deb
dpkg -i gcli_package.deb
```

## Usage

If using a different runtime, update the metadata for the client to compile:

	subxt metadata -f bytes > res/metadata.scale

Send 10 ĞD from Alice to Ferdie:

	cargo run -- --url ws://localhost:9944 --secret //Alice account transfer 1000 5CiPPseXPECbkjWCa6MnjNokrgYjMqmKndv2rSnekmSK2DjL

List certifications and session keys that will expire within one month:

	cargo run -- --url wss://gdev.p2p.legal:443/ws smith expire --blocks 432000

For more examples see [in the doc](./doc). `cargo run --` is replaced by `gcli` as if the binary was added to your path.

#### Log level

You can adjust the log level:

```
export RUST_LOG=gcli=info
```

#### Runtime metadata

To update runtime metadata:

```
subxt metadata -f bytes > res/metadata.scale
```

### Smith

You want to rotate keys and go online to start forging blocks.

Smith nodes must not expose a public RPC API. Then you can either use SSH directly, or make an SSH bridge by adding this to your local `.bashrc`:

```bash
alias duniter-rpc='ssh -L 9944:localhost:9944 duniter@gdev.example.tld'
```

Now the command `duniter-rpc` will open an SSH session and a bridge to your RPC API.

When your node is ready to forge blocks, rotate keys and go online:

```bash
gcli --secret "my secret phrase" smith update-keys
gcli --secret "my secret phrase" smith go-online
```

### Keys

Secret and/or public keys can always be passed using `--secret` and `--address`. If needed, stdin will be prompted for secret key. An error will occur if secret and address are both given but do not match.

Secret key format can be changed using `--secret-format` with the following values:
* `substrate`: a Substrate secret address (optionally followed by a derivation path), or BIP39 mnemonic
* `seed`: a 32-bytes seed in hexadecimal (Duniter v1 compatible)

## Changelog

For a detailed list of changes, see the [CHANGELOG](./CHANGELOG.md).